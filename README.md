# Lung Segmenation for Chest CT #

Perform lungs segmentation in Chest CT images

## Prerequisites
```
skImage
nibabel
pytorch
dipy
scipy
numpy
glob
os 
sys
```

### How to run the script? ###
The script `lung_segmentation.py`  contains a function called `segment_lungs` it takes a nibabel image as input and outputs a nibabel image containing the binary segmentation mask.

See the commented lines at the bottom of the function definition.