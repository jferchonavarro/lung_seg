test:

python3 test.py INPUT OUTPUT --modelname R231
python3 test.py INPUT OUTPUT --modelname LTRCLobes
python3 test.py INPUT OUTPUT --modelname R231CovidWeb

python3 test.py ../../tr_im.nii.gz ../result.nii.gz (default model is  R231)

train:

python3 train.py INPUT OUTPUT --modelname R231   (select model to finetune)
python3 train.py INPUT OUTPUT --modelname LTRCLobes
python3 train.py INPUT OUTPUT --modelname R231CovidWeb

python3 train.py ../../tr_im.nii.gz ../result.nii.gz (default model is  R231)

